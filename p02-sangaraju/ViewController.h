//
//  ViewController.h
//  p02-sangaraju
//
//  Created by Rameshkumarraju Sangaraju on 2/7/16.
//  Copyright © 2016 rsangar1. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (retain, nonatomic)IBOutletCollection(UILabel) NSArray *labelsArray;
@property (weak, nonatomic) IBOutlet UITextField *scoreField;
@property (weak, nonatomic) IBOutlet UILabel *labelScore;
@property (weak, nonatomic) IBOutlet UILabel *labelGameOver;
@property (weak, nonatomic) IBOutlet UIButton *tryAgainButton;


@property (weak, nonatomic) IBOutlet UILabel *labelR1C1;
@property (weak, nonatomic) IBOutlet UILabel *labelR1C2;
@property (weak, nonatomic) IBOutlet UILabel *labelR1C3;
@property (weak, nonatomic) IBOutlet UILabel *labelR1C4;

@property (weak, nonatomic) IBOutlet UILabel *labelR2C1;
@property (weak, nonatomic) IBOutlet UILabel *labelR2C2;
@property (weak, nonatomic) IBOutlet UILabel *labelR2C3;
@property (weak, nonatomic) IBOutlet UILabel *labelR2C4;

@property (weak, nonatomic) IBOutlet UILabel *labelR3C1;
@property (weak, nonatomic) IBOutlet UILabel *labelR3C2;
@property (weak, nonatomic) IBOutlet UILabel *labelR3C3;
@property (weak, nonatomic) IBOutlet UILabel *labelR3C4;

@property (weak, nonatomic) IBOutlet UILabel *labelR4C1;
@property (weak, nonatomic) IBOutlet UILabel *labelR4C2;
@property (weak, nonatomic) IBOutlet UILabel *labelR4C3;
@property (weak, nonatomic) IBOutlet UILabel *labelR4C4;



@property (weak, nonatomic) IBOutlet UIButton *upButton;
@property (weak, nonatomic) IBOutlet UIButton *rightButton;
@property (weak, nonatomic) IBOutlet UIButton *leftButton;
@property (weak, nonatomic) IBOutlet UIButton *downButton;


@end

