//
//  ViewController.m
//  p02-sangaraju
//
//  Created by Rameshkumarraju Sangaraju on 2/7/16.
//  Copyright © 2016 rsangar1. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize labelsArray;

bool gameOver= NO;
int score = 0;
UILabel *currentLabel;
UILabel *label2;
UILabel *label3;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    _scoreField.text = @"0";
    UILabel *label1 = labelsArray[[self getRandomNumber]];
    label1.text = @"2";
    UILabel *label2 = labelsArray[[self getRandomNumber]];
    label2.text = @"2";
    if(label1 == label2){
        label1.text = @"4";
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (int)getRandomNumber{
    //there will be total of 16 boxes, so should get number between 0 and 15
    int randomNumber = arc4random()%16;
    return randomNumber;
}

//- (IBAction)<#selector#>:(id)sender

- (void)upButtonPressed {
    //column1
    for(int index = 0; index < 12; index = index + 4){
        currentLabel = labelsArray[index];
        label2 = labelsArray[index+4];
        if(![currentLabel.text isEqualToString:@""] && ![label2.text isEqualToString:@""]){
            if([currentLabel.text isEqualToString:label2.text]){
                int newValue = 2* [currentLabel.text integerValue];
                score = score + newValue;
                _scoreField.text = [NSString stringWithFormat:@"%d", score];
                currentLabel.text = [NSString stringWithFormat:@"%d", newValue];
                label2.text = @"";
            }
        }
    }
    
    for(int index = 0; index < 12; index = index + 4){
        currentLabel = labelsArray[index];
        label2 = labelsArray[index+4];
        if(index<=4){
            label3 = labelsArray[index+8];
            if([currentLabel.text isEqualToString:@""] && [label2.text isEqualToString:@""] && ![label3.text isEqualToString:@""]){
                currentLabel.text = label3.text;
                label3.text = @"";
            }
        }
        if([currentLabel.text isEqualToString:@""] && ![label2.text isEqualToString:@""]){
            currentLabel.text = label2.text;
            label2.text = @"";
        }
    }
    
    //column2
    for(int index = 1; index < 13; index=index+4){
        currentLabel = labelsArray[index];
        label2 = labelsArray[index+4];
        if(![currentLabel.text isEqualToString:@""] && ![label2.text isEqualToString:@""]){
            if([currentLabel.text isEqualToString:label2.text]){
                int newValue = 2* [currentLabel.text integerValue];
                score = score + newValue;
                _scoreField.text = [NSString stringWithFormat:@"%d", score];
                currentLabel.text = [NSString stringWithFormat:@"%d", newValue];
                label2.text = @"";
            }
        }
    }
    
    for(int index = 1; index < 13; index=index+4){
        currentLabel = labelsArray[index];
        label2 = labelsArray[index+4];
        if(index<=5){
            label3 = labelsArray[index+8];
            if([currentLabel.text isEqualToString:@""] && [label2.text isEqualToString:@""] && ![label3.text isEqualToString:@""]){
                currentLabel.text = label3.text;
                label3.text = @"";
            }
        }
        if([currentLabel.text isEqualToString:@""] && ![label2.text isEqualToString:@""]){
            currentLabel.text = label2.text;
            label2.text = @"";
        }
    }
    
    //column3
    for(int index = 2; index < 14; index=index+4){
        currentLabel = labelsArray[index];
        label2 = labelsArray[index+4];
        if(![currentLabel.text isEqualToString:@""] && ![label2.text isEqualToString:@""]){
            if([currentLabel.text isEqualToString:label2.text]){
                int newValue = 2* [currentLabel.text integerValue];
                score = score + newValue;
                _scoreField.text = [NSString stringWithFormat:@"%d", score];
                currentLabel.text = [NSString stringWithFormat:@"%d", newValue];
                label2.text = @"";
            }
        }
    }
    
    for(int index = 2; index < 14; index=index+4){
        currentLabel = labelsArray[index];
        label2 = labelsArray[index+4];
        if(index<=6){
            label3 = labelsArray[index+8];
            if([currentLabel.text isEqualToString:@""] && [label2.text isEqualToString:@""] && ![label3.text isEqualToString:@""]){
                currentLabel.text = label3.text;
                label3.text = @"";
            }
        }
        if([currentLabel.text isEqualToString:@""] && ![label2.text isEqualToString:@""]){
            currentLabel.text = label2.text;
            label2.text = @"";
        }
    }
    
    //column4
    for(int index = 3; index < 15; index=index+4){
        currentLabel = labelsArray[index];
        label2 = labelsArray[index+4];
        if(![currentLabel.text isEqualToString:@""] && ![label2.text isEqualToString:@""]){
            if([currentLabel.text isEqualToString:label2.text]){
                int newValue = 2* [currentLabel.text integerValue];
                score = score + newValue;
                _scoreField.text = [NSString stringWithFormat:@"%d", score];
                currentLabel.text = [NSString stringWithFormat:@"%d", newValue];
                label2.text = @"";
            }
        }
    }
    
    for(int index = 3; index < 15; index=index+4){
        currentLabel = labelsArray[index];
        label2 = labelsArray[index+4];
        if(index<=7){
            label3 = labelsArray[index+8];
            if([currentLabel.text isEqualToString:@""] && [label2.text isEqualToString:@""] && ![label3.text isEqualToString:@""]){
                currentLabel.text = label3.text;
                label3.text = @"";
            }
        }
        if([currentLabel.text isEqualToString:@""] && ![label2.text isEqualToString:@""]){
            currentLabel.text = label2.text;
            label2.text = @"";
        }
    }
    
    
    if(![self isGameOver])
        [self pickNewRandomLabel];
    else
        [self gameFinished];

}

- (void)rightButtonPressed {
    //row1
    for(int index = 3; index > 0; index--){
        currentLabel = labelsArray[index];
        label2 = labelsArray[index-1];
        if(![currentLabel.text isEqualToString:@""] && ![label2.text isEqualToString:@""]){
            if([currentLabel.text isEqualToString:label2.text]){
                int newValue = 2* [currentLabel.text integerValue];
                score = score + newValue;
                _scoreField.text = [NSString stringWithFormat:@"%d", score];
                currentLabel.text = [NSString stringWithFormat:@"%d", newValue];
                label2.text = @"";
            }
        }
    }
    
    for(int index = 3; index > 0; index--){
        currentLabel = labelsArray[index];
        label2 = labelsArray[index-1];
        if(index>=2){
            label3 = labelsArray[index-2];
            if([currentLabel.text isEqualToString:@""] && [label2.text isEqualToString:@""] && ![label3.text isEqualToString:@""]){
                currentLabel.text = label3.text;
                label3.text = @"";
            }
        }
        if([currentLabel.text isEqualToString:@""] && ![label2.text isEqualToString:@""]){
            currentLabel.text = label2.text;
            label2.text = @"";
        }
    }
    
    //row2
    for(int index = 7; index > 4; index--){
        currentLabel = labelsArray[index];
        label2 = labelsArray[index-1];
        if(![currentLabel.text isEqualToString:@""] && ![label2.text isEqualToString:@""]){
            if([currentLabel.text isEqualToString:label2.text]){
                int newValue = 2* [currentLabel.text integerValue];
                score = score + newValue;
                _scoreField.text = [NSString stringWithFormat:@"%d", score];
                currentLabel.text = [NSString stringWithFormat:@"%d", newValue];
                label2.text = @"";
            }
        }
    }
    
    for(int index = 7; index > 4; index--){
        currentLabel = labelsArray[index];
        label2 = labelsArray[index-1];
        if(index>=6){
            label3 = labelsArray[index-2];
            if([currentLabel.text isEqualToString:@""] && [label2.text isEqualToString:@""] && ![label3.text isEqualToString:@""]){
                currentLabel.text = label3.text;
                label3.text = @"";
            }
        }
        if([currentLabel.text isEqualToString:@""] && ![label2.text isEqualToString:@""]){
            currentLabel.text = label2.text;
            label2.text = @"";
        }
    }
    
    //row3
    for(int index = 11; index > 8; index--){
        currentLabel = labelsArray[index];
        label2 = labelsArray[index-1];
        if(![currentLabel.text isEqualToString:@""] && ![label2.text isEqualToString:@""]){
            if([currentLabel.text isEqualToString:label2.text]){
                int newValue = 2* [currentLabel.text integerValue];
                score = score + newValue;
                _scoreField.text = [NSString stringWithFormat:@"%d", score];
                currentLabel.text = [NSString stringWithFormat:@"%d", newValue];
                label2.text = @"";
            }
        }
    }
    
    for(int index = 11; index > 8; index--){
        currentLabel = labelsArray[index];
        label2 = labelsArray[index-1];
        if(index>=10){
            label3 = labelsArray[index-2];
            if([currentLabel.text isEqualToString:@""] && [label2.text isEqualToString:@""] && ![label3.text isEqualToString:@""]){
                currentLabel.text = label3.text;
                label3.text = @"";
            }
        }
        if([currentLabel.text isEqualToString:@""] && ![label2.text isEqualToString:@""]){
            currentLabel.text = label2.text;
            label2.text = @"";
        }
    }
    
    //row4
    for(int index = 15; index > 12; index--){
        currentLabel = labelsArray[index];
        label2 = labelsArray[index-1];
        if(![currentLabel.text isEqualToString:@""] && ![label2.text isEqualToString:@""]){
            if([currentLabel.text isEqualToString:label2.text]){
                int newValue = 2* [currentLabel.text integerValue];
                score = score + newValue;
                _scoreField.text = [NSString stringWithFormat:@"%d", score];
                currentLabel.text = [NSString stringWithFormat:@"%d", newValue];
                label2.text = @"";
            }
        }
    }
    
    for(int index = 15; index > 12; index--){
        currentLabel = labelsArray[index];
        label2 = labelsArray[index-1];
        if(index>=14){
            label3 = labelsArray[index-2];
            if([currentLabel.text isEqualToString:@""] && [label2.text isEqualToString:@""] && ![label3.text isEqualToString:@""]){
                currentLabel.text = label3.text;
                label3.text = @"";
            }
        }
        if([currentLabel.text isEqualToString:@""] && ![label2.text isEqualToString:@""]){
            currentLabel.text = label2.text;
            label2.text = @"";
        }
    }
    
    
    if(![self isGameOver])
        [self pickNewRandomLabel];
    else
        [self gameFinished];
}

- (void)leftButtonPressed {
    
    //row1
    for(int index = 0; index < 3; index++){
        currentLabel = labelsArray[index];
        label2 = labelsArray[index+1];
        if(![currentLabel.text isEqualToString:@""] && ![label2.text isEqualToString:@""]){
            if([currentLabel.text isEqualToString:label2.text]){
                int newValue = 2* [currentLabel.text integerValue];
                score = score + newValue;
                _scoreField.text = [NSString stringWithFormat:@"%d", score];
                currentLabel.text = [NSString stringWithFormat:@"%d", newValue];
                label2.text = @"";
            }
        }
    }
    
    for(int index = 0; index < 3; index++){
        currentLabel = labelsArray[index];
        label2 = labelsArray[index+1];
        if(index<=1){
            label3 = labelsArray[index+2];
            if([currentLabel.text isEqualToString:@""] && [label2.text isEqualToString:@""] && ![label3.text isEqualToString:@""]){
                currentLabel.text = label3.text;
                label3.text = @"";
            }
        }
        if([currentLabel.text isEqualToString:@""] && ![label2.text isEqualToString:@""]){
            currentLabel.text = label2.text;
            label2.text = @"";
        }
    }
    
    //row2
    for(int index = 4; index < 7; index++){
        currentLabel = labelsArray[index];
        label2 = labelsArray[index+1];
        if(![currentLabel.text isEqualToString:@""] && ![label2.text isEqualToString:@""]){
            if([currentLabel.text isEqualToString:label2.text]){
                int newValue = 2* [currentLabel.text integerValue];
                score = score + newValue;
                _scoreField.text = [NSString stringWithFormat:@"%d", score];
                currentLabel.text = [NSString stringWithFormat:@"%d", newValue];
                label2.text = @"";
            }
        }
    }
    
    for(int index = 4; index < 7; index++){
        currentLabel = labelsArray[index];
        label2 = labelsArray[index+1];
        if(index<=5){
            label3 = labelsArray[index+2];
            if([currentLabel.text isEqualToString:@""] && [label2.text isEqualToString:@""] && ![label3.text isEqualToString:@""]){
                currentLabel.text = label3.text;
                label3.text = @"";
            }
        }
        if([currentLabel.text isEqualToString:@""] && ![label2.text isEqualToString:@""]){
            currentLabel.text = label2.text;
            label2.text = @"";
        }
    }
    
    //row3
    for(int index = 8; index < 11; index++){
        currentLabel = labelsArray[index];
        label2 = labelsArray[index+1];
        if(![currentLabel.text isEqualToString:@""] && ![label2.text isEqualToString:@""]){
            if([currentLabel.text isEqualToString:label2.text]){
                int newValue = 2* [currentLabel.text integerValue];
                score = score + newValue;
                _scoreField.text = [NSString stringWithFormat:@"%d", score];
                currentLabel.text = [NSString stringWithFormat:@"%d", newValue];
                label2.text = @"";
            }
        }
    }
    
    for(int index = 8; index < 11; index++){
        currentLabel = labelsArray[index];
        label2 = labelsArray[index+1];
        if(index<=9){
            label3 = labelsArray[index+2];
            if([currentLabel.text isEqualToString:@""] && [label2.text isEqualToString:@""] && ![label3.text isEqualToString:@""]){
                currentLabel.text = label3.text;
                label3.text = @"";
            }
        }
        if([currentLabel.text isEqualToString:@""] && ![label2.text isEqualToString:@""]){
            currentLabel.text = label2.text;
            label2.text = @"";
        }
    }

    //row4
    for(int index = 12; index < 15; index++){
        currentLabel = labelsArray[index];
        label2 = labelsArray[index+1];
        if(![currentLabel.text isEqualToString:@""] && ![label2.text isEqualToString:@""]){
            if([currentLabel.text isEqualToString:label2.text]){
                int newValue = 2* [currentLabel.text integerValue];
                score = score + newValue;
                _scoreField.text = [NSString stringWithFormat:@"%d", score];
                currentLabel.text = [NSString stringWithFormat:@"%d", newValue];
                label2.text = @"";
            }
        }
    }
    
    for(int index = 12; index < 15; index++){
        currentLabel = labelsArray[index];
        label2 = labelsArray[index+1];
        if(index<=13){
            label3 = labelsArray[index+2];
            if([currentLabel.text isEqualToString:@""] && [label2.text isEqualToString:@""] && ![label3.text isEqualToString:@""]){
                currentLabel.text = label3.text;
                label3.text = @"";
            }
        }
        if([currentLabel.text isEqualToString:@""] && ![label2.text isEqualToString:@""]){
            currentLabel.text = label2.text;
            label2.text = @"";
        }
    }
    
    
    if(![self isGameOver])
        [self pickNewRandomLabel];
    else
        [self gameFinished];
}

- (void)downButtonPressed {
    //column1
    for(int index = 12; index > 0; index = index - 4){
        currentLabel = labelsArray[index];
        label2 = labelsArray[index-4];
        if(![currentLabel.text isEqualToString:@""] && ![label2.text isEqualToString:@""]){
            if([currentLabel.text isEqualToString:label2.text]){
                int newValue = 2* [currentLabel.text integerValue];
                score = score + newValue;
                _scoreField.text = [NSString stringWithFormat:@"%d", score];
                currentLabel.text = [NSString stringWithFormat:@"%d", newValue];
                label2.text = @"";
            }
        }
    }
    
    for(int index = 12; index > 0; index = index - 4){
        currentLabel = labelsArray[index];
        label2 = labelsArray[index-4];
        if(index>=8){
            label3 = labelsArray[index-8];
            if([currentLabel.text isEqualToString:@""] && [label2.text isEqualToString:@""] && ![label3.text isEqualToString:@""]){
                currentLabel.text = label3.text;
                label3.text = @"";
            }
        }
        if([currentLabel.text isEqualToString:@""] && ![label2.text isEqualToString:@""]){
            currentLabel.text = label2.text;
            label2.text = @"";
        }
    }
    
    //column2
    for(int index = 13; index >1; index=index-4){
        currentLabel = labelsArray[index];
        label2 = labelsArray[index-4];
        if(![currentLabel.text isEqualToString:@""] && ![label2.text isEqualToString:@""]){
            if([currentLabel.text isEqualToString:label2.text]){
                int newValue = 2* [currentLabel.text integerValue];
                score = score + newValue;
                _scoreField.text = [NSString stringWithFormat:@"%d", score];
                currentLabel.text = [NSString stringWithFormat:@"%d", newValue];
                label2.text = @"";
            }
        }
    }
    
    for(int index = 13; index >1; index=index-4){
        currentLabel = labelsArray[index];
        label2 = labelsArray[index-4];
        if(index>=9){
            label3 = labelsArray[index-8];
            if([currentLabel.text isEqualToString:@""] && [label2.text isEqualToString:@""] && ![label3.text isEqualToString:@""]){
                currentLabel.text = label3.text;
                label3.text = @"";
            }
        }
        if([currentLabel.text isEqualToString:@""] && ![label2.text isEqualToString:@""]){
            currentLabel.text = label2.text;
            label2.text = @"";
        }
    }
    
    //column3
    for(int index = 14; index > 2; index=index-4){
        currentLabel = labelsArray[index];
        label2 = labelsArray[index-4];
        if(![currentLabel.text isEqualToString:@""] && ![label2.text isEqualToString:@""]){
            if([currentLabel.text isEqualToString:label2.text]){
                int newValue = 2* [currentLabel.text integerValue];
                score = score + newValue;
                _scoreField.text = [NSString stringWithFormat:@"%d", score];
                currentLabel.text = [NSString stringWithFormat:@"%d", newValue];
                label2.text = @"";
            }
        }
    }
    
    for(int index = 14; index > 2; index=index-4){
        currentLabel = labelsArray[index];
        label2 = labelsArray[index-4];
        if(index>=10){
            label3 = labelsArray[index-8];
            if([currentLabel.text isEqualToString:@""] && [label2.text isEqualToString:@""] && ![label3.text isEqualToString:@""]){
                currentLabel.text = label3.text;
                label3.text = @"";
            }
        }
        if([currentLabel.text isEqualToString:@""] && ![label2.text isEqualToString:@""]){
            currentLabel.text = label2.text;
            label2.text = @"";
        }
    }
    
    //column4
    for(int index = 15; index > 3; index=index-4){
        currentLabel = labelsArray[index];
        label2 = labelsArray[index-4];
        if(![currentLabel.text isEqualToString:@""] && ![label2.text isEqualToString:@""]){
            if([currentLabel.text isEqualToString:label2.text]){
                int newValue = 2* [currentLabel.text integerValue];
                score = score + newValue;
                _scoreField.text = [NSString stringWithFormat:@"%d", score];
                currentLabel.text = [NSString stringWithFormat:@"%d", newValue];
                label2.text = @"";
            }
        }
    }
    
    for(int index = 15; index > 3; index=index-4){
        currentLabel = labelsArray[index];
        label2 = labelsArray[index-4];
        if(index>=11){
            label3 = labelsArray[index-8];
            if([currentLabel.text isEqualToString:@""] && [label2.text isEqualToString:@""] && ![label3.text isEqualToString:@""]){
                currentLabel.text = label3.text;
                label3.text = @"";
            }
        }
        if([currentLabel.text isEqualToString:@""] && ![label2.text isEqualToString:@""]){
            currentLabel.text = label2.text;
            label2.text = @"";
        }
    }
    
    
    if(![self isGameOver])
        [self pickNewRandomLabel];
    else
        [self gameFinished];
}

- (void) pickNewRandomLabel{
    int randomNumber = [self getRandomNumber];
    UILabel *randomLabel = labelsArray[randomNumber];
    while(![randomLabel.text isEqualToString: @""]){
        randomLabel = labelsArray[[self getRandomNumber]];
    }
    randomLabel.text = @"2";
}

- (BOOL) isGameOver{
    int count = 0;
    for(int i=0;i<=15;i++){
        UILabel *currentLabel = labelsArray[i];
        if(![currentLabel.text isEqualToString: @""]){
            count = count + 1;
        }else{
            gameOver = NO;
            break;
        }
    }
    if(count >=15){
        gameOver = YES;
    }
    return gameOver;
}

- (void) gameFinished{
    _labelGameOver.hidden = NO;
    _tryAgainButton.hidden = NO;
}

- (void) resetGame{
    for(int i=0;i<=15;i++){
        UILabel *thisLabel = labelsArray[i];
        thisLabel.text = @"";
    }
    _labelGameOver.hidden = YES;
    _tryAgainButton.hidden = YES;
    [self viewDidLoad];
}

// from stackoverflow
// Ref: http://stackoverflow.com/questions/5858247/ios-one-ibaction-for-multiple-buttons

-(IBAction)buttonAction:(id)sender {
    switch ([sender tag]) {
        case 1:
            [self upButtonPressed];
            break;
        case 2:
            [self rightButtonPressed];
            break;
        case 3:
            [self leftButtonPressed];
            break;
        case 4:
            [self downButtonPressed];
            break;
        case 5:
            [self resetGame];
            break;
        default:
            break;
    }
}
@end
