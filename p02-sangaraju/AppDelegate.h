//
//  AppDelegate.h
//  p02-sangaraju
//
//  Created by Rameshkumarraju Sangaraju on 2/7/16.
//  Copyright © 2016 rsangar1. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

